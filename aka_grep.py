#!/usr/bin/python

import argparse
import re

parser = argparse.ArgumentParser(description='Grep script')

parser.add_argument('pattern', action="store")
parser.add_argument('files', action="store", nargs='+')
parser.add_argument('-n', action='store_true', dest='printNumberLines')
parser.add_argument('-f', action='store_true', dest='printNameFiles')
parser.add_argument('-i', action='store_true', dest='printIgnorecaseLines')
parser.add_argument('-v', action='store_true', dest='printInvertLines')

args = parser.parse_args()
print(args)


def grepFile(file, pattern):
    count = 0
    with open(file) as fp:
        while True:
            count += 1
            line = fp.readline()
            if not line:
                break
            if re.search(pattern, line):
                print("{}: {}".format(count, line.strip()))


def grepFilePrintNumber(file, pattern):
    count = 0
    with open(file) as fp:
        while True:
            count += 1
            line = fp.readline()
            if not line:
                break
            if re.search(pattern, line):
                print(count, end=', '),


def grepFilePrintFile(file, pattern):
    with open(file) as fp:
        while True:
            line = fp.readline()
            if not line:
                break
            if re.search(pattern, line):
                print(file)
                break


def grepFileIgnorecase(file, pattern):
    count = 0
    with open(file) as fp:
        while True:
            count += 1
            line = fp.readline()
            if not line:
                break
            if re.search(pattern, line, re.IGNORECASE):
                print("{}: {}".format(count, line.strip()))


def grepFileInvert(file, pattern):
    count = 0
    with open(file) as fp:
        while True:
            count += 1
            line = fp.readline()
            if not line:
                break
            if line != "\n":
                if not re.search(pattern, line):
                    print("{}: {}".format(count, line.strip()))


for file in args.files:
    if args.printNumberLines:
        grepFilePrintNumber(file, args.pattern)
    elif args.printNameFiles:
        grepFilePrintFile(file, args.pattern)
    elif args.printIgnorecaseLines:
        grepFileIgnorecase(file, args.pattern)
    elif args.printInvertLines:
        grepFileInvert(file, args.pattern)
    else:
        grepFile(file, args.pattern)

