#!/usr/bin/python

import psutil
import socket
import time

processName = "nginx"
ip = "127.0.0.1"
port = 80
retry = 5
delay = 5
timeout = 3


def checkIfProcessRunning(processName):
    for proc in psutil.process_iter():
        try:
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False


def isOpen(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(timeout)
    try:
        s.connect((ip, int(port)))
        s.shutdown(socket.SHUT_RDWR)
        return True
    except:
        return False
    finally:
        s.close()


def checkHost(ip, port):
    retryCheck = 0
    for i in range(retry):
        if isOpen(ip, port):
            break
        else:
            retryCheck += 1
            time.sleep(delay)
    return retryCheck


if checkIfProcessRunning(processName):
    retryCheck = checkHost(ip, port)
    if retryCheck == 0:
        print("OK")
    elif retryCheck < retry:
        print("Warning")
    else:
        print("Error")
else:
    print("Error")
